const path = require('path')
const CompressionPlugin = require('compression-webpack-plugin')

module.exports = {
  configureWebpack: {
    resolve: {
      alias: {
        '@': path.resolve(__dirname, 'src/'),
      },
    },
  },
  chainWebpack: function (config) {
    config.plugins.delete('prefetch')
    config.plugin('CompressionPlugin').use(CompressionPlugin)
    config.plugins.delete('progress')
  },
  transpileDependencies: ['vuex-persist'],
  devServer: {
    port: 3000,
    disableHostCheck: true, // process.env.NODE_ENV === 'development'
    useLocalIp: false,
    progress: false,
  },
  pwa: {
    workboxOptions: {
      skipWaiting: true,
    },
  },
}
