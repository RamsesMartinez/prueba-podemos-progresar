import Vue from 'vue'
import VueRouter from 'vue-router'
import authRoutes from '@/modules/auth/router'
import usersRoutes from '@/modules/users/router'
import clientesRoutes from '@/modules/clientes/router'
import gruposRoutes from '@/modules/grupos/router'
import store from '@/store'

Vue.use(VueRouter)

let baseRoutes = [
  {
    path: '*',
    name: 'index',
    redirect: {
      name: 'authLogin',
    },
  },
]

// noinspection JSCheckFunctionSignatures
const routes = baseRoutes.concat(
  authRoutes,
  usersRoutes,
  clientesRoutes,
  gruposRoutes
)

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
})

router.beforeEach((to, from, next) => {
  document.title = to.meta.title
  const requiresAuth = to.matched.some((record) => record.meta.requiresAuth)
  const isLogged = store.getters['auth/isLogged']

  if (requiresAuth && !isLogged) {
    next({ name: 'authLogin' })
  }

  next()
})

export default router
