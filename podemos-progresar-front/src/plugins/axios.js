import Vue from 'vue'
import axios from 'axios'
import store from '../store'
import router from '../router'
import VueAxios from 'vue-axios'

require('promise.prototype.finally').shim()

const baseURL =
  process.env.NODE_ENV === 'development'
    ? 'http://127.0.0.1:5000/api/'
    : `${process.env.VUE_APP_BACKEND_API}/api/`

axios.defaults.baseURL = baseURL

// Request interceptor
axios.interceptors.request.use(
  (config) => {
    const tokenAccess = store.state['auth']['tokenAccess']
    const tokenRefresh = store.state['auth']['tokenRefresh']

    switch (config.url) {
      case `/auth/login`:
        return config
      case `/auth/refresh`:
        config.headers['Authorization'] = `Bearer ${tokenRefresh}`
        return config
    }
    if (tokenAccess) {
      config.headers['Authorization'] = `Bearer ${tokenAccess}`
    }
    return config
  },
  (error) => {
    // noinspection JSIgnoredPromiseFromCall
    Promise.reject(error)
  }
)

// Response interceptor

axios.interceptors.response.use(
  (response) => {
    return response
  },
  function (error) {
    const originalRequest = error.config

    if (
      error.response.status === 401 &&
      originalRequest.url === `/auth/refresh/`
    ) {
      store.commit('auth/clearTokens')

      // noinspection JSIgnoredPromiseFromCall
      router
        .push({
          name: 'authLogin',
        })
        .catch(() => {})
      return Promise.reject(error)
    }

    if (error.response.status === 401 && !originalRequest._retry) {
      originalRequest._retry = true
      store.dispatch('auth/refreshToken').then((res) => {
        if (res.status === 200) {
          const tokenAccess = res.data['access_token']
          store.commit('auth/setTokenAccess', tokenAccess)
          return axios(originalRequest)
        } else {
          originalRequest._retry = false
        }
      })
    }
    return Promise.reject(error)
  }
)
Vue.use(VueAxios, axios)
