import DatePicker from 'vue2-datepicker'
import Vue from 'vue'
import 'vue2-datepicker/index.css'
import 'vue2-datepicker/locale/es'

Vue.component('DatePicker', DatePicker)
