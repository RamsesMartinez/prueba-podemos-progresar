import Vue from 'vue'
import Vuex from 'vuex'
import auth from '@/modules/auth/store'
import users from '@/modules/users/store'
import clientes from '@/modules/clientes/store'
import grupos from '@/modules/grupos/store'

import VuexPersistence from 'vuex-persist'
Vue.use(Vuex)

const vuexLocal = new VuexPersistence({
  storage: window.localStorage,
  modules: ['auth', 'patients', 'consultingroom'],
})
export default new Vuex.Store({
  state: {
    activeLoading: false,
    loadingPageMsg: '',
    errorMsg: '',
    skeletonLoading: true,
  },
  mutations: {
    setLoading(state, { isLoading, msg }) {
      state.activeLoading = isLoading
      state.loadingPageMsg = msg
    },
    setSkeletonLoading: (state, isLoading) => {
      state.skeletonLoading = isLoading
    },
  },
  modules: {
    auth,
    users,
    clientes,
    grupos,
  },
  plugins: [vuexLocal.plugin],
})
