import axios from 'axios'

export async function getClientesAction({ commit }) {
  // Set loading page animation
  commit(
    'setLoading',
    {
      isLoading: true,
      msg: 'Obteniendo lista de clientes',
    },
    { root: true }
  )
  return new Promise((resolve, reject) => {
    let url = `/v1/clientes`
    axios
      .get(url)
      .then((response) => {
        resolve(response.data)
      })
      .catch((error) => {
        reject(error)
      })
      .finally(() => {
        // Finish loading page animation
        commit('setLoading', false, { root: true })
      })
  })
}
