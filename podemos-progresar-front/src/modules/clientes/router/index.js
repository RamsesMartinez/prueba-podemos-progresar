const ClientesView = () =>
  import(
    /* webpackChunkName: "clientes" */ '@/modules/clientes/views/ClientesView'
  )

export default [
  {
    path: '/clientes',
    name: 'clientesList',
    meta: {
      title: 'Panel de clientes',
      requiresAuth: true,
    },
    component: ClientesView,
  },
]
