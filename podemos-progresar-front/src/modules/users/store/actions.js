import axios from 'axios'

export async function updateUser({ commit }, { data, username }) {
  // Set loading page animation
  commit(
    'setLoading',
    {
      isLoading: true,
      msg: 'Actualizando datos de usuario',
    },
    { root: true }
  )
  return new Promise((resolve, reject) => {
    let url = `/users/${username}/`
    axios
      .patch(url, data)
      .then((response) => {
        resolve(response.data)
      })
      .catch((error) => {
        reject(error)
      })
      .finally(() => {
        // Finish loading page animation
        commit('setLoading', false, { root: true })
      })
  })
}

export async function updateUserProfile({ commit }, { data, username }) {
  // Set loading page animation
  commit(
    'setLoading',
    {
      isLoading: true,
      msg: 'Actualizando perfil del usuario',
    },
    { root: true }
  )
  return new Promise((resolve, reject) => {
    let url = `/users/${username}/profile/`
    axios
      .patch(url, data)
      .then((response) => {
        resolve(response.data)
      })
      .catch((error) => {
        reject(error)
      })
      .finally(() => {
        // Finish loading page animation
        commit('setLoading', false, { root: true })
      })
  })
}

/**
 * Método de obtención de usuario.
 *
 * @returns {Promise<Object>}
 */
// eslint-disable-next-line no-unused-vars
export async function getSubscription({ commit }, username) {
  return new Promise((resolve, reject) => {
    let url = `/users/${username}/`
    axios
      .get(url)
      .then((response) => {
        resolve(response.data['user']['userprofile']['subscription'])
      })
      .catch((error) => {
        reject(error)
      })
  })
}

/**
 * Método de obtención de usuarios del sistema.
 *
 * @param params params Parámetros para configurar los friltos de búsqueda.
 * @returns {Promise<Object>}
 */
export async function getUsers({ commit }, params = null) {
  // Set loading page animation
  commit(
    'setLoading',
    {
      isLoading: true,
      msg: 'Obteniendo usuarios',
    },
    { root: true }
  )
  return new Promise((resolve, reject) => {
    let url = `/users/`
    url = params !== null ? `${url}?${params}` : url
    axios
      .get(url)
      .then((response) => {
        resolve(response.data)
      })
      .catch((error) => {
        reject(error)
      })
      .finally(() => {
        // Finish loading page animation
        commit('setLoading', false, { root: true })
      })
  })
}
