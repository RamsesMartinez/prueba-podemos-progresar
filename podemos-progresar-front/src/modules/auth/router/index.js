const LoginView = () =>
  import(/* webpackChunkName: "auth" */ '@/modules/auth/views/LoginView')

export default [
  {
    path: '/auth/login',
    name: 'authLogin',
    meta: {
      title: 'Iniciar Sesión',
      requiresAuth: false,
    },
    component: LoginView,
  },
]
