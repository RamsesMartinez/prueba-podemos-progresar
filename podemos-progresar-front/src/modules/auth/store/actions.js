import axios from 'axios'

export async function getToken({ commit }, user) {
  // Set loading page animation
  commit(
    'setLoading',
    {
      isLoading: true,
      msg: 'Iniciando sesión',
    },
    { root: true }
  )

  return new Promise((resolve, reject) => {
    axios
      .post('/auth/login', user)
      .then((response) => {
        const data = response['data']
        const tokenAccess = data['access_token']
        const tokenRefresh = data['refresh_token']
        commit('setUserTokens', {
          tokenAccess,
          tokenRefresh,
        })
        resolve(response)
      })
      .catch((error) => {
        reject(error)
      })
      .finally(() => {
        // Finish loading page animation
        commit('setLoading', false, { root: true })
      })
  })
}

// eslint-disable-next-line no-unused-vars
export async function refreshToken({ commit, state }) {
  return new Promise((resolve, reject) => {
    axios
      .post('/auth/refresh', { refresh: state.tokenRefresh })
      .then((response) => {
        resolve(response)
      })
      .catch((error) => {
        reject(error)
      })
  })
}

// eslint-disable-next-line object-curly-newline
export async function logout({ commit }) {
  commit('logout')
}

export async function singUp({ commit }, data) {
  // Set loading page animation
  commit(
    'setLoading',
    {
      isLoading: true,
      msg: 'Registrando usuario',
    },
    { root: true }
  )
  return new Promise((resolve, reject) => {
    axios
      .post('/auth/signup/', data)
      .then((response) => {
        resolve(response !== undefined)
      })
      .catch((error) => {
        reject(error)
      })
      .finally(() => {
        // Finish loading page animation
        commit('setLoading', false, { root: true })
      })
  })
}
