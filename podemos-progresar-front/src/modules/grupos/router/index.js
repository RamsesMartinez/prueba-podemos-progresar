const GruposView = () =>
  import(/* webpackChunkName: "grupos" */ '@/modules/grupos/views/GruposView')

const GroupDetailView = () =>
  import(
    /* webpackChunkName: "grupos" */ '@/modules/grupos/views/GroupDetailView'
  )

export default [
  {
    path: '/grupos',
    name: 'gruposList',
    meta: {
      title: 'Panel de Grupos',
      requiresAuth: true,
    },
    component: GruposView,
  },
  {
    path: '/grupos/:id',
    name: 'GroupDetailView',
    meta: {
      title: 'Detalles del grupo',
      requiresAuth: true,
    },
    component: GroupDetailView,
  },
]
