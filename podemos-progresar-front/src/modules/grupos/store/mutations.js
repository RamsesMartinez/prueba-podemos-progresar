export default {
  setGrupos: (state, grupos) => {
    state.grupos = grupos
  },
  setCuentas: (state, cuentas) => {
    state.cuentas = cuentas
  },
}
