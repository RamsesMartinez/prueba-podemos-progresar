import axios from 'axios'

export async function getGruposAction({ commit }) {
  // Set loading page animation
  commit(
    'setLoading',
    {
      isLoading: true,
      msg: 'Obteniendo lista de grupos',
    },
    { root: true }
  )
  return new Promise((resolve, reject) => {
    let url = `/v1/grupos`
    axios
      .get(url)
      .then((response) => {
        resolve(response.data)
      })
      .catch((error) => {
        reject(error)
      })
      .finally(() => {
        // Finish loading page animation
        commit('setLoading', false, { root: true })
      })
  })
}
export async function getCuentasAction({ commit }, grupoId) {
  // Set loading page animation
  commit(
    'setLoading',
    {
      isLoading: true,
      msg: 'Obteniendo lista de cuentas',
    },
    { root: true }
  )
  return new Promise((resolve, reject) => {
    let url = `/v1/cuentas?grupo=${grupoId}`
    axios
      .get(url)
      .then((response) => {
        resolve(response.data)
      })
      .catch((error) => {
        reject(error)
      })
      .finally(() => {
        // Finish loading page animation
        commit('setLoading', false, { root: true })
      })
  })
}
