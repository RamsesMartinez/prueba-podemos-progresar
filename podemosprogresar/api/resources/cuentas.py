from flask import request
from flask_restful import Resource
from flask_jwt_extended import jwt_required

from podemosprogresar.api.schemas import CuentaSchema
from podemosprogresar.extensions import db
from podemosprogresar.commons.pagination import paginate
from podemosprogresar.models import Cuentas


class CuentaResource(Resource):
    """Single object resource

    ---
    get:
      tags:
        - api
      parameters:
        - in: path
          name: cuenta_id
          schema:
            type: string
      responses:
        200:
          content:
            application/json:
              schema:
                type: object
                properties:
                  cuenta: CuentaSchema
        404:
          description: Cuenta no existe
    put:
      tags:
        - api
      parameters:
        - in: path
          name: cuenta_id
          schema:
            type: string
      requestBody:
        content:
          application/json:
            schema:
              CuentaSchema
      responses:
        200:
          content:
            application/json:
              schema:
                type: object
                properties:
                  msg:
                    type: string
                    example: Cuenta actualizada
                  cuenta: CuentaSchema
        404:
          description: Cuenta no existe
    delete:
      tags:
        - api
      parameters:
        - in: path
          name: cuenta_id
          schema:
            type: string
      responses:
        200:
          content:
            application/json:
              schema:
                type: object
                properties:
                  msg:
                    type: string
                    example: Cuenta eliminada
        404:
          description: Cuenta no existe
    """

    method_decorators = [jwt_required]

    def get(self, cuenta_id):
        schema = CuentaSchema()
        cuenta = Cuentas.query.get_or_404(cuenta_id)
        return {"cuenta": schema.dump(cuenta)}

    def put(self, cuenta_id):
        schema = CuentaSchema(partial=True)
        cuenta = Cuentas.query.get_or_404(cuenta_id)
        cuenta = schema.load(request.json, instance=cuenta)

        db.session.commit()

        return {"msg": "Cuenta actualizada", "cuenta": schema.dump(cuenta)}

    def delete(self, cuenta_id):
        cuenta = Cuentas.query.get_or_404(cuenta_id)
        db.session.delete(cuenta)
        db.session.commit()

        return {"msg": "Cuenta eliminada"}


class CuentaList(Resource):
    """Creation and get_all

    ---
    get:
      tags:
        - api
      responses:
        200:
          content:
            application/json:
              schema:
                allOf:
                  - $ref: '#/components/schemas/PaginatedResult'
                  - type: object
                    properties:
                      results:
                        type: array
                        items:
                          $ref: '#/components/schemas/CuentaSchema'
    post:
      tags:
        - api
      requestBody:
        content:
          application/json:
            schema:
              CuentaSchema
      responses:
        201:
          content:
            application/json:
              schema:
                type: object
                properties:
                  msg:
                    type: string
                    example: Cuenta creada
                  cuenta: CuentaSchema
    """

    method_decorators = [jwt_required]

    def get(self):
        schema = CuentaSchema(many=True)
        args = request.args
        grupo = None
        try:
            grupo = args['grupo']
        except Exception:
            pass
        if grupo is not None:
            query = Cuentas.query.filter_by(grupo_id=grupo)
        else:
            query = Cuentas.query

        return paginate(query, schema)

    def post(self):
        schema = CuentaSchema()
        cuenta = schema.load(request.json)

        db.session.add(cuenta)
        db.session.commit()

        return {"msg": "Cuenta creada", "cuenta": schema.dump(cuenta)}, 201
