from flask import request
from flask_restful import Resource
from flask_jwt_extended import jwt_required

from podemosprogresar.api.schemas import GrupoSchema
from podemosprogresar.extensions import db
from podemosprogresar.commons.pagination import paginate
from podemosprogresar.models import Grupos


class GrupoResource(Resource):
    """Single object resource

    ---
    get:
      tags:
        - api
      parameters:
        - in: path
          name: grupo_id
          schema:
            type: string
      responses:
        200:
          content:
            application/json:
              schema:
                type: object
                properties:
                  grupo: GrupoSchema
        404:
          description: Grupo no existe
    put:
      tags:
        - api
      parameters:
        - in: path
          name: grupo_id
          schema:
            type: string
      requestBody:
        content:
          application/json:
            schema:
              GrupoSchema
      responses:
        200:
          content:
            application/json:
              schema:
                type: object
                properties:
                  msg:
                    type: string
                    example: Grupo actualizado
                  grupo: GrupoSchema
        404:
          description: Grupo no existe
    delete:
      tags:
        - api
      parameters:
        - in: path
          name: grupo_id
          schema:
            type: string
      responses:
        200:
          content:
            application/json:
              schema:
                type: object
                properties:
                  msg:
                    type: string
                    example: Grupo eliminado
        404:
          description: Grupo no existe
    """

    method_decorators = [jwt_required]

    def get(self, grupo_id):
        schema = GrupoSchema()
        grupo = Grupos.query.get_or_404(grupo_id)
        return {"grupo": schema.dump(grupo)}

    def put(self, grupo_id):
        schema = GrupoSchema(partial=True)
        grupo = Grupos.query.get_or_404(grupo_id)
        grupo = schema.load(request.json, instance=grupo)

        db.session.commit()

        return {"msg": "Grupo actualizado", "grupo": schema.dump(grupo)}

    def delete(self, grupo_id):
        grupo = Grupos.query.get_or_404(grupo_id)
        db.session.delete(grupo)
        db.session.commit()

        return {"msg": "Grupo eliminado"}


class GrupoList(Resource):
    """Creation and get_all

    ---
    get:
      tags:
        - api
      responses:
        200:
          content:
            application/json:
              schema:
                allOf:
                  - $ref: '#/components/schemas/PaginatedResult'
                  - type: object
                    properties:
                      results:
                        type: array
                        items:
                          $ref: '#/components/schemas/GrupoSchema'
    post:
      tags:
        - api
      requestBody:
        content:
          application/json:
            schema:
              GrupoSchema
      responses:
        201:
          content:
            application/json:
              schema:
                type: object
                properties:
                  msg:
                    type: string
                    example: Grupo creado
                  grupo: GrupoSchema
    """

    method_decorators = [jwt_required]

    def get(self):
        schema = GrupoSchema(many=True)
        query = Grupos.query
        return paginate(query, schema)

    def post(self):
        schema = GrupoSchema()
        grupo = schema.load(request.json)

        db.session.add(grupo)
        db.session.commit()

        return {"msg": "Grupo creado", "grupo": schema.dump(grupo)}, 201
