from flask import request
from flask_restful import Resource
from flask_jwt_extended import jwt_required

from podemosprogresar.api.schemas import TransaccionSchema
from podemosprogresar.extensions import db
from podemosprogresar.commons.pagination import paginate
from podemosprogresar.models import Transacciones


class TransaccionResource(Resource):
    """Single object resource

    ---
    get:
      tags:
        - api
      parameters:
        - in: path
          name: transaccion_id
          schema:
            type: integer
      responses:
        200:
          content:
            application/json:
              schema:
                type: object
                properties:
                  transaccion: TransaccionSchema
        404:
          description: Transaccion no existe
    put:
      tags:
        - api
      parameters:
        - in: path
          name: transaccion_id
          schema:
            type: integer
      requestBody:
        content:
          application/json:
            schema:
              TransaccionSchema
      responses:
        200:
          content:
            application/json:
              schema:
                type: object
                properties:
                  msg:
                    type: integer
                    example: Transacción actualizada
                  transaccion: TransaccionSchema
        404:
          description: Transaccion no existe
    delete:
      tags:
        - api
      parameters:
        - in: path
          name: transaccion_id
          schema:
            type: integer
      responses:
        200:
          content:
            application/json:
              schema:
                type: object
                properties:
                  msg:
                    type: integer
                    example: Transacción eliminada
        404:
          description: Transaccion no existe
    """

    method_decorators = [jwt_required]

    def get(self, transaccion_id):
        schema = TransaccionSchema()
        transaccion = Transacciones.query.get_or_404(transaccion_id)
        return {"transaccion": schema.dump(transaccion)}

    def put(self, transaccion_id):
        schema = TransaccionSchema(partial=True)
        transaccion = Transacciones.query.get_or_404(transaccion_id)
        transaccion = schema.load(request.json, instance=transaccion)

        db.session.commit()

        return {"msg": "Transacción actualizada", "transaccion": schema.dump(transaccion)}

    def delete(self, transaccion_id):
        transaccion = Transacciones.query.get_or_404(transaccion_id)
        db.session.delete(transaccion)
        db.session.commit()

        return {"msg": "Transacción eliminada"}


class TransaccionList(Resource):
    """Creation and get_all

    ---
    get:
      tags:
        - api
      responses:
        200:
          content:
            application/json:
              schema:
                allOf:
                  - $ref: '#/components/schemas/PaginatedResult'
                  - type: object
                    properties:
                      results:
                        type: array
                        items:
                          $ref: '#/components/schemas/TransaccionSchema'
    post:
      tags:
        - api
      requestBody:
        content:
          application/json:
            schema:
              TransaccionSchema
      responses:
        201:
          content:
            application/json:
              schema:
                type: object
                properties:
                  msg:
                    type: integer
                    example: Transacción creada
                  transaccion: TransaccionSchema
    """

    method_decorators = [jwt_required]

    def get(self):
        schema = TransaccionSchema(many=True)
        query = Transacciones.query
        return paginate(query, schema)

    def post(self):
        schema = TransaccionSchema()
        transaccion = schema.load(request.json)

        db.session.add(transaccion)
        db.session.commit()

        return {"msg": "Transacción creada", "transaccion": schema.dump(transaccion)}, 201
