from flask import request
from flask_restful import Resource
from flask_jwt_extended import jwt_required
from podemosprogresar.api.schemas.calendario_pagos import CalendarioPagosSchema
from podemosprogresar.models import CalendarioPagos
from podemosprogresar.extensions import db
from podemosprogresar.commons.pagination import paginate


class CalendarioPagosResource(Resource):
    """Single object resource

    ---
    get:
      tags:
        - api
      parameters:
        - in: path
          name: calendario_pagos_id
          schema:
            type: integer
      responses:
        200:
          content:
            application/json:
              schema:
                type: object
                properties:
                  calendarioPagos: CalendarioPagosSchema
        404:
          description: Calendario de pagos no existe
    put:
      tags:
        - api
      parameters:
        - in: path
          name: calendario_pagos_id
          schema:
            type: integer
      requestBody:
        content:
          application/json:
            schema:
              CalendarioPagosSchema
      responses:
        200:
          content:
            application/json:
              schema:
                type: object
                properties:
                  msg:
                    type: string
                    example: Calendario de pagos actualizado
                  calendarioPagos: CalendarioPagosSchema
        404:
          description: Calendario de pagos no existe
    delete:
      tags:
        - api
      parameters:
        - in: path
          name: calendario_pagos_id
          schema:
            type: integer
      responses:
        200:
          content:
            application/json:
              schema:
                type: object
                properties:
                  msg:
                    type: string
                    example: Calendario de pagos eliminado
        404:
          description: Calendario de pagos no existe
    """

    method_decorators = [jwt_required]

    def get(self, calendario_pagos_id):
        schema = CalendarioPagosSchema()
        calendario_pagos = CalendarioPagos.query.get_or_404(calendario_pagos_id)
        return {"calendarioPagos": schema.dump(calendario_pagos)}

    def put(self, calendario_pagos_id):
        schema = CalendarioPagosSchema(partial=True)
        calendario_pagos = CalendarioPagos.query.get_or_404(calendario_pagos_id)
        calendario_pagos = schema.load(request.json, instance=calendario_pagos)

        db.session.commit()

        return {"msg": "Calendario de pagos actualizado", "calendarioPagos": schema.dump(calendario_pagos)}

    def delete(self, calendario_pagos_id):
        calendario_pagos = CalendarioPagos.query.get_or_404(calendario_pagos_id)
        db.session.delete(calendario_pagos)
        db.session.commit()

        return {"msg": "Calendario de pagos eliminado"}


class CalendarioPagosList(Resource):
    """Creation and get_all

    ---
    get:
      tags:
        - api
      responses:
        200:
          content:
            application/json:
              schema:
                allOf:
                  - $ref: '#/components/schemas/PaginatedResult'
                  - type: object
                    properties:
                      results:
                        type: array
                        items:
                          $ref: '#/components/schemas/CalendarioPagosSchema'
    post:
      tags:
        - api
      requestBody:
        content:
          application/json:
            schema:
              CalendarioPagosSchema
      responses:
        201:
          content:
            application/json:
              schema:
                type: object
                properties:
                  msg:
                    type: string
                    example: Calendario de pagos creado
                  user: CalendarioPagosSchema
    """

    method_decorators = [jwt_required]

    def get(self):
        schema = CalendarioPagosSchema(many=True)
        query = CalendarioPagos.query
        return paginate(query, schema)

    def post(self):
        schema = CalendarioPagosSchema()
        calendario_pagos = schema.load(request.json)

        db.session.add(calendario_pagos)
        db.session.commit()

        return {"msg": "Calendario de pagos creado", "calendarioPagos": schema.dump(calendario_pagos)}, 201
