from podemosprogresar.api.resources.user import UserResource, UserList  # noqa
from podemosprogresar.api.resources.calendario_pagos import CalendarioPagosResource, CalendarioPagosList  # noqa
from podemosprogresar.api.resources.clientes import ClienteResource, ClienteList  # noqa
from podemosprogresar.api.resources.cuentas import CuentaResource, CuentaList  # noqa
from podemosprogresar.api.resources.grupos import GrupoResource, GrupoList  # noqa
from podemosprogresar.api.resources.transacciones import TransaccionResource, TransaccionList  # noqa

users = [
    "UserResource",
    "UserList",
]

calendario_pagos = [
    "CalendarioPagosResource",
    "CalendarioPagosList"
]

clientes = [
 "ClienteResource",
 "ClienteList"
]

cuentas = [
 "CuentaResource",
 "CuentaList",
]

grupos = [
 "GrupoResource",
 "GrupoList",
]

transacciones = [
 "TransaccionResource",
 "TransaccionList",
]

__all__ = users + calendario_pagos + clientes + cuentas + grupos + transacciones
