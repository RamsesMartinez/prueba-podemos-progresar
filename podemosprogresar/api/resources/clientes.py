from flask import request
from flask_restful import Resource
from flask_jwt_extended import jwt_required

from podemosprogresar.api.schemas import ClienteSchema
from podemosprogresar.extensions import db
from podemosprogresar.commons.pagination import paginate
from podemosprogresar.models import Clientes


class ClienteResource(Resource):
    """Single object resource

    ---
    get:
      tags:
        - api
      parameters:
        - in: path
          name: cliente_id
          schema:
            type: string
      responses:
        200:
          content:
            application/json:
              schema:
                type: object
                properties:
                  cliente: ClienteSchema
        404:
          description: Cliente no existe
    put:
      tags:
        - api
      parameters:
        - in: path
          name: cliente_id
          schema:
            type: string
      requestBody:
        content:
          application/json:
            schema:
              ClienteSchema
      responses:
        200:
          content:
            application/json:
              schema:
                type: object
                properties:
                  msg:
                    type: string
                    example: Cliente actualizado
                  cliente: ClienteSchema
        404:
          description: Cliente no existe
    delete:
      tags:
        - api
      parameters:
        - in: path
          name: cliente_id
          schema:
            type: string
      responses:
        200:
          content:
            application/json:
              schema:
                type: object
                properties:
                  msg:
                    type: string
                    example: Cliente eliminado
        404:
          description: Cliente no existe
    """

    method_decorators = [jwt_required]

    def get(self, cliente_id):
        schema = ClienteSchema()
        cliente = Clientes.query.get_or_404(cliente_id)
        return {"cliente": schema.dump(cliente)}

    def put(self, cliente_id):
        schema = ClienteSchema(partial=True)
        cliente = Clientes.query.get_or_404(cliente_id)
        cliente = schema.load(request.json, instance=cliente)

        db.session.commit()

        return {"msg": "Cliente actualizado", "cliente": schema.dump(cliente)}

    def delete(self, cliente_id):
        cliente = Clientes.query.get_or_404(cliente_id)
        db.session.delete(cliente)
        db.session.commit()

        return {"msg": "Cliente eliminado"}


class ClienteList(Resource):
    """Creation and get_all

    ---
    get:
      tags:
        - api
      responses:
        200:
          content:
            application/json:
              schema:
                allOf:
                  - $ref: '#/components/schemas/PaginatedResult'
                  - type: object
                    properties:
                      results:
                        type: array
                        items:
                          $ref: '#/components/schemas/ClienteSchema'
    post:
      tags:
        - api
      requestBody:
        content:
          application/json:
            schema:
              ClienteSchema
      responses:
        201:
          content:
            application/json:
              schema:
                type: object
                properties:
                  msg:
                    type: string
                    example: Cliente creado
                  user: ClienteSchema
    """

    method_decorators = [jwt_required]

    def get(self):
        schema = ClienteSchema(many=True)
        query = Clientes.query
        return paginate(query, schema)

    def post(self):
        schema = ClienteSchema()
        cliente = schema.load(request.json)

        db.session.add(cliente)
        db.session.commit()

        return {"msg": "Cliente creado", "cliente": schema.dump(cliente)}, 201
