from flask import Blueprint, current_app, jsonify
from flask_restful import Api
from marshmallow import ValidationError

from podemosprogresar.api.resources.calendario_pagos import CalendarioPagosResource, CalendarioPagosList
from podemosprogresar.extensions import apispec
from podemosprogresar.api.resources import UserResource, UserList, ClienteResource, ClienteList, CuentaResource, \
    CuentaList, GrupoResource, GrupoList, TransaccionResource, TransaccionList
from podemosprogresar.api.schemas import UserSchema


blueprint = Blueprint("api", __name__, url_prefix="/api/v1")
api = Api(blueprint)


# Calendario pagos
api.add_resource(
    CalendarioPagosResource,
    "/calendario-pagos/<int:calendario_pagos_id>",
    endpoint="calendario_de_pagos_by_id"
)
api.add_resource(CalendarioPagosList, "/calendario-pagos", endpoint="calendario_de_pagos")

# Clientes
api.add_resource(ClienteResource, "/clientes/<string:cliente_id>", endpoint="cliente_by_id")
api.add_resource(ClienteList, "/clientes", endpoint="clientes")

# Cuentas
api.add_resource(CuentaResource, "/cuentas/<string:cuenta_id>", endpoint="cuenta_by_id")
api.add_resource(CuentaList, "/cuentas", endpoint="cuentas")

# Grupos
api.add_resource(GrupoResource, "/grupos/<string:grupo_id>", endpoint="grupo_by_id")
api.add_resource(GrupoList, "/grupos", endpoint="grupos")

# Transacciones
api.add_resource(TransaccionResource, "/transacciones/<int:transaccion_id>", endpoint="transaccion_by_id")
api.add_resource(TransaccionList, "/transacciones", endpoint="transacciones")

# Users
api.add_resource(UserResource, "/users/<int:user_id>", endpoint="user_by_id")
api.add_resource(UserList, "/users", endpoint="users")


# noinspection DuplicatedCode
@blueprint.before_app_first_request
def register_views():
    apispec.spec.components.schema("UserSchema", schema=UserSchema)
    apispec.spec.path(view=UserResource, app=current_app)
    apispec.spec.path(view=UserList, app=current_app)
    apispec.spec.path(view=CalendarioPagosResource, app=current_app)
    apispec.spec.path(view=CalendarioPagosList, app=current_app)
    apispec.spec.path(view=ClienteResource, app=current_app)
    apispec.spec.path(view=ClienteList, app=current_app)
    apispec.spec.path(view=CuentaResource, app=current_app)
    apispec.spec.path(view=CuentaList, app=current_app)
    apispec.spec.path(view=GrupoResource, app=current_app)
    apispec.spec.path(view=GrupoList, app=current_app)
    apispec.spec.path(view=TransaccionResource, app=current_app)
    apispec.spec.path(view=TransaccionList, app=current_app)


@blueprint.errorhandler(ValidationError)
def handle_marshmallow_error(e):
    """Return json error for marshmallow validation errors.

    This will avoid having to try/catch ValidationErrors in all endpoints, returning
    correct JSON response with associated HTTP 400 Status (https://tools.ietf.org/html/rfc7231#section-6.5.1)
    """
    return jsonify(e.messages), 400


@blueprint.after_request  # blueprint can also be app~~
def after_request(response):
    header = response.headers
    header['Access-Control-Allow-Origin'] = '*'
    header['Access-Control-Allow-Headers'] = '*'
    return response
