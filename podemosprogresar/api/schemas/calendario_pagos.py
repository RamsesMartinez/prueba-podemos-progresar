from podemosprogresar.models import CalendarioPagos
from podemosprogresar.extensions import ma, db


class CalendarioPagosSchema(ma.SQLAlchemyAutoSchema):
    monto = ma.Float()

    class Meta:
        model = CalendarioPagos
        sqla_session = db.session
        load_instance = True
        include_relationships = True
