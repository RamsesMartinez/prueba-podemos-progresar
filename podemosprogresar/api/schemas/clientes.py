from podemosprogresar.models import Clientes
from podemosprogresar.extensions import ma, db


class ClienteSchema(ma.SQLAlchemyAutoSchema):

    class Meta:
        model = Clientes
        sqla_session = db.session
        load_instance = True
        include_relationships = True
