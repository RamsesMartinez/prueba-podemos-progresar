from podemosprogresar.models import Grupos, Clientes
from podemosprogresar.extensions import ma, db


class ClienteGrupoSchema(ma.SQLAlchemyAutoSchema):

    class Meta:
        model = Clientes
        sqla_session = db.session
        load_instance = True


class GrupoSchema(ma.SQLAlchemyAutoSchema):
    clientes = ma.Nested(ClienteGrupoSchema, many=True)

    class Meta:
        model = Grupos
        sqla_session = db.session
        load_instance = True
        include_relationships = True
