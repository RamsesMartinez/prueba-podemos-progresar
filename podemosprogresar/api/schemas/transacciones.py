from podemosprogresar.models import Transacciones
from podemosprogresar.extensions import ma, db


class TransaccionSchema(ma.SQLAlchemyAutoSchema):

    monto = ma.Float()

    class Meta:
        model = Transacciones
        sqla_session = db.session
        load_instance = True
        include_relationships = True
