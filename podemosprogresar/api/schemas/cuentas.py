from podemosprogresar.models import Cuentas
from podemosprogresar.extensions import ma, db


class CuentaSchema(ma.SQLAlchemyAutoSchema):

    monto = ma.Float()
    saldo = ma.Float()

    class Meta:
        model = Cuentas
        sqla_session = db.session
        load_instance = True
        include_relationships = True
