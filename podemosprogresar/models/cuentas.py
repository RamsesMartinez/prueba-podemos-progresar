from sqlalchemy.orm import relationship

from podemosprogresar.extensions import db


class Cuentas(db.Model):
    __tablename__ = 'Cuentas'

    id = db.Column(db.String(5), primary_key=True)
    grupo_id = db.Column(db.ForeignKey('Grupos.id'), nullable=False, index=True)
    estatus = db.Column(db.String(15), nullable=False)
    monto = db.Column(db.DECIMAL(15, 2), nullable=False)
    saldo = db.Column(db.DECIMAL(15, 2), nullable=False)

    grupo = relationship('Grupos')
