from podemosprogresar.extensions import db

from sqlalchemy.orm import relationship


class CalendarioPagos(db.Model):
    __tablename__ = 'CalendarioPagos'

    id = db.Column(db.INTEGER(), primary_key=True)
    cuenta_id = db.Column(db.ForeignKey('Cuentas.id'), nullable=False, index=True)
    num_pago = db.Column(db.Integer(), nullable=False)
    monto = db.Column(db.DECIMAL(15, 2), nullable=False)
    fecha_pago = db.Column(db.Date, nullable=False)
    estatus = db.Column(db.String(15), nullable=False)

    cuenta = relationship('Cuentas')
