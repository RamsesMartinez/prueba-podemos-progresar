from sqlalchemy.orm import relationship

from podemosprogresar.extensions import db


class Transacciones(db.Model):
    __tablename__ = 'Transacciones'

    id = db.Column(db.Integer(), primary_key=True)
    cuenta_id = db.Column(db.ForeignKey('Cuentas.id'), nullable=False, index=True)
    fecha = db.Column(db.DateTime, nullable=False)
    monto = db.Column(db.DECIMAL(15, 2), nullable=False)

    cuenta = relationship('Cuentas')
