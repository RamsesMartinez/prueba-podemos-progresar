from podemosprogresar.extensions import db


class Clientes(db.Model):
    __tablename__ = 'Clientes'

    id = db.Column(db.String(7), primary_key=True)
    nombre = db.Column(db.String(60), nullable=False)
