from sqlalchemy.ext.declarative import declarative_base

from podemosprogresar.extensions import db

Base = declarative_base()
metadata = Base.metadata


t_Miembros = db.Table(
    'Miembros',
    db.Column('grupo_id', db.ForeignKey('Grupos.id'), primary_key=True, nullable=False),
    db.Column('cliente_id', db.ForeignKey('Clientes.id'), primary_key=True, nullable=False, index=True),
    db.PrimaryKeyConstraint('grupo_id', 'cliente_id')
)


class Grupos(db.Model):
    __tablename__ = 'Grupos'

    id = db.Column(
        db.String(5),
        primary_key=True
    )
    nombre = db.Column(
        db.String(20),
        nullable=False
    )

    clientes = db.relationship('Clientes', secondary="Miembros", backref='Grupos', lazy="joined")
