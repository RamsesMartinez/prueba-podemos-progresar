load data infile '/var/lib/mysql-files/001_data_clientes.csv' into table podemos_eval.Clientes
    fields terminated by ','
    ENCLOSED BY '"'
    LINES TERMINATED BY '\n'
    IGNORE 1 LINES;
load data infile '/var/lib/mysql-files/002_data_grupos.csv' into table podemos_eval.Grupos
    fields terminated by ','
    ENCLOSED BY '"'
    LINES TERMINATED BY '\n'
    IGNORE 1 LINES;
load data infile '/var/lib/mysql-files/003_data_miembros.csv' into table podemos_eval.Miembros
    fields terminated by ','
    ENCLOSED BY '"'
    LINES TERMINATED BY '\n'
    IGNORE 1 LINES;
load data infile '/var/lib/mysql-files/004_data_cuentas.csv' into table podemos_eval.Cuentas
    fields terminated by ','
    ENCLOSED BY '"'
    LINES TERMINATED BY '\n'
    IGNORE 1 LINES;
load data infile '/var/lib/mysql-files/005_data_calendariopagos.csv' into table podemos_eval.CalendarioPagos
    fields terminated by ','
    ENCLOSED BY '"'
    LINES TERMINATED BY '\n'
    IGNORE 1 LINES;
load data infile '/var/lib/mysql-files/006_data_transacciones.csv' into table podemos_eval.Transacciones
    fields terminated by ','
    ENCLOSED BY '"'
    LINES TERMINATED BY '\n'
    IGNORE 1 LINES;
