# Prueba Podemos Progresar

Proyecto de prueba para podemos progresar.

- Este proyecto fue creado con el cookiecutter [cookiecutter-flask-restful](https://github.com/karec/cookiecutter-flask-restful)
y adaptado a las necesidades particulares del mismo.


**Nota Importante**

La rama master, contiene todos los cambios más recientes hasta el día Jueves 10 de Septiembre, como se acordó, a la par,
se continuó puliendo últimos detalles, los cuáles han sido realizados posteriores a esa fecha, si igualmente se desea
revisar la versión más reciente del proyecto, sólo hay que bajar la rama `develop`.

Gracias.

## Comenzar y ejecutar localmente con Docker

Estas instrucciones le proporcionarán una copia del proyecto en funcionamiento en su máquina local con fines de desarrollo y prueba.

### Prerequisitos

- Docker 18+; Puede consultar las [instrucciones oficiales de instalación aquí](https://docs.docker.com/get-docker/) para su SO;
- Docker Compose 1.18+; consulte la documentación oficial para [obtener la guía de instalación](https://docs.docker.com/compose/install/).
- NodeJs (12+, < 14), Sólo si trabajará con el ambiente de desarrollo sin ejecutar el contenedor de node. [Puede descargarlo desde aquí](https://nodejs.org/dist/v12.18.3/node-v12.18.3-linux-x64.tar.xz).
    - Yarn 1.20+. Puede descargarlo [desde aquí](https://classic.yarnpkg.com/en/docs/install/#debian-stable). 
### Inicio rápido

Se incluye un archivo Makefile para ejecutar tareas comunes, entre las que destacan:

- build: Genera las primeras imágenes docker del proyecto.
- run: Levanta los contenedores del proyecto en segundo plano.
- init. Ejecuta los comandos `make build`, `make run`, inicializa tablas y valores en la BDD y posteriormente levanta el servicio para la interfaz web.
- db-migrate: Genera nuevas migraciones, en caso de existir cambios en los modelos.
- db-upgrade: Replica los cambios de los modelos en la BDD.

Para poder visualizar el proyecto en su última versión simplemente ejecute el comando `init`:

```bash
make init
```

Esto puede llevar un tiempo, en particular, la primera vez que ejecuta este comando en su sistema de desarrollo.

**Nota:** Es probable que la primera vez en ser ejecutado el comando `make init`, 
muestre un error casi al final de la generación del proyecto, similar al siguiente:

```bash
...
sqlalchemy.exc.OperationalError: (pymysql.err.OperationalError) (2003, "Can't connect to MySQL server on 'mysql' ([Errno 111] Connection refused)")
(Background on this error at: http://sqlalche.me/e/13/e3q8)
make: *** [Makefile:4: init] Error 1
```   

Esto se debe a que el servicio de `mysql` no ha terminado de ser desplegado (mas no buildedado) 
y crear las primeras tablas para después poblarlas.

Para resolverlo, espere unos segundos y ejecute el siguiente comando para aplicar las migraciones pendientes 
sin recompilar el proyecto y levantar el servicio:

```bash
make init-no-build
```

A continuación podrá visualizar la interfaz web en la siguiente dirección: [localhost:3000](http://localhost:3000) accediendo con las sisguientes credenciales:
- username: **admin**
- password: **admin**

Una vez haya detenido los revicios y quiera volver a levantar el proyecto, ejecute el siguiente comando:

```bash
make run
```

Ya que si levanta el proyecto con el comando `make init`, se volverán a crear de nuevo los contenedores, 
así como también, se cargarán nuevamente ciertos datos en la BDD. 

### Construcción del Stack

Este comando de docker, generará las imágenes iniciales, requeridas para el proyecto.

```bash
docker-compose build
```

### Ejecute el Stack

Este paso levantará el contenedor del back con flask, el contenedor de MySQL y el contenedor de Node para el front.
La primera vez que se ejecuta, puede llevar un tiempo en comenzar, pero las ejecuciones posteriores se realizarán rápidamente.

Abra una terminal en la raíz del proyecto y ejecute lo siguiente:

```bash
docker-compose up
```

O bien, para ejecutar el proyecto en el background, simplemente ejecute:

```bash
docker-compose up -d
```

A continuación, se levantarán los contenedores ya indicados, además de que el contenedor de `mysql`,
automáticamente creará los esquemas iniciales con los scripts indicados en la carpeta `mysql_initial_schemas`.

De igual forma, poblará las tablas del proyecto con los archivos `csv` indicados en la carpeta `fixtures`; 

### Ejecutar comandos de administración

Al igual que con cualquier comando de shell que deseemos ejecutar en nuestro contenedor, esto se hace 
ejecutando el comando `docker-compose run --rm <YOUR SERVICE CONTAINER> <YOUR COMMAND>`:

Aplicar primeras migraciones e inicializar el primer usuario default:

```bash
docker-compose run --rm web podemosprogresar db upgrade
docker-compose run --rm web podemosprogresar init
```

Se observa que, `web` es el servicio de destino (definido en el docker-compose.yml) contra el que estamos ejecutando los comandos.

#### Resultado migraciones

La base de datos se actualizará, de tal forma que se agregarán las tablas necesarias para el funcionamiento de flask, 
así como también renombrará las llaves foráneas, index's, etc, de tal forma de que tengan un nombre estandarizado.

El resultado será el siguiente:

![Diagrama final BDD](diagrama%20final%20podemos%20progresar.png)

*Esquema obtenido con MySQL Workbench, una vez inicializado el proyecto.*

El usuario creado, está definido en el archivo `podemosprogresar/manage.py`, el cuál, está creado a partir del siguiente objeto:
contendrá los siguientes valores en la tabla `users`:

```python
User(username="admin", email="admin@admin.com", password="admin", active=True)
```

## Configuring the Environment

Este es el extracto del `docker-compose.yml` de nuestro proyecto:

```bash
# ...
  mysql:
    image: "mysql/mysql-server:5.7"
    volumes:
      - ./mysql_initial_schemas:/docker-entrypoint-initdb.d
      - local_mysql_data:/var/lib/mysql
      - ./fixtures/:/var/lib/mysql-files
    env_file:
      - ./.envs/.local/.mysql.env
    ports:
      - "3310:3306"
# ...
```

Lo más importante para nosotros, es la sección env_file que incluye `./.envs/.local/.mysql.env`.

Por default, se incorporaron los archivos con las variables de entorno para cada servicio de docker, 
los cuáles están commiteados en git por comodidad, además de ser un ambiente de desarrollo.

Para casos concretos de un despliegue del proyecto en un ambiente de producción, se recomienda tener los archivos de 
Dockerfile con configuraciones concretas para el ambiente productivo. 

```bash
.envs
└── .local
   ├── .flask.env
   └── .mysql.env
```

**Nota**: Las credenciales incluídas por default en el archivo `.envs/.local/.mysql.env`, pueden ser modificadas a conveniencia,
siempre y cuando se incorporen en la primer ejecución del proyecto, ya que es ahí donde se generan los volúmenes de docker.

```bash
MYSQL_ROOT_PASSWORD=root
MYSQL_USER=admin
MYSQL_PASSWORD=password
MYSQL_LOG_CONSOLE=true
```

Puede consultar las demás variable de entorno en la documentación oficial de la [imagen de mysql en DockerHub](https://hub.docker.com/_/mysql).


## APISpec y Swagger

Esta aplicación, utiliza el formato OpenAPI en su versión 3, puede acceder a culaquier de los siguientes 2 enlaces 
para ver las especificaciones de los endpoints:

- Swagger-UI: [/swagger-ui](http://localhost:5000/swagger-ui)
- Swagger.json: [/swagger.json](http://localhost:5000/swagge.json)

## Test cases

Para ejecutar el set de pruebas, ejecute el siguiente comando

```shell
make test
```

## Autor

- **Ramses Martinez** - _Initial work_ - [RamsesMartinez](https://github.com/RamsesMartinez)

## License

This project is licensed under the MIT License - see the LICENSE file for details
