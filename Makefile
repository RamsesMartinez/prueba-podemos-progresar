.PHONY: init init-no-run init-migration build run db-migrate test tox

init:  build run
	docker-compose run --rm web podemosprogresar db upgrade
	docker-compose run --rm web podemosprogresar init
	@echo "Init done, containers running"

init-no-build:
	docker-compose run --rm web podemosprogresar db upgrade
	docker-compose run --rm web podemosprogresar init
	@echo "Init done, containers running"

build:
	docker-compose build

run:
	docker-compose up -d

db-migrate:
	docker-compose run --rm web podemosprogresar db migrate

db-upgrade:
	docker-compose run --rm web podemosprogresar db upgrade

test:
	docker-compose run -v $(PWD)/tests:/code/tests:ro web tox -e test

tox:
	docker-compose run -v $(PWD)/tests:/code/tests:ro web tox -e py38

lint:
	docker-compose run web tox -e lint
